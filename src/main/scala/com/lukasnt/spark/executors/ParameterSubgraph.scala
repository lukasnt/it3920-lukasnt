package com.lukasnt.spark.executors

import com.lukasnt.spark.models.Types.{AttrEdge, AttrVertex, Properties, TemporalGraph}
import com.lukasnt.spark.queries.ParameterQuery

class ParameterSubgraph(parameterQuery: ParameterQuery) extends SubgraphExecutor {

  private val sourcePredicate: AttrVertex => Boolean      = parameterQuery.sourcePredicate
  private val intermediatePredicate: AttrEdge => Boolean  = parameterQuery.intermediatePredicate
  private val destinationPredicate: AttrVertex => Boolean = parameterQuery.destinationPredicate

  /**
    * The subgraph function that filters the graph to keep only the vertices and edges that are source or destination or pass the intermediate predicate.
    * In addition, maps the graph such that the Pregel phase can use the source and destination properties on vertices.
    * @param temporalGraph The graph to filter
    * @return The filtered graph
    */
  override def subgraph(temporalGraph: TemporalGraph): TemporalGraph = {
    // Map the graph first to add source and destination properties such that the Pregel phase can use them
    // Then filter the graph to keep only the vertices that are source or destination or pass the intermediate predicate
    val result = temporalGraph
      .mapVertices(
        (id, attr) =>
          new Properties(
            attr.interval,
            attr.typeLabel,
            attr.properties +
              ("source"     -> sourcePredicate(AttrVertex(id, attr)).toString,
              "destination" -> destinationPredicate(AttrVertex(id, attr)).toString)
        ))
    println(s"vertices: ${result.vertices.count()}, edges: ${result.edges.count()}")
    result
  }

}

object ParameterSubgraph {

  def apply(temporalGraph: TemporalGraph, parameterQuery: ParameterQuery): TemporalGraph =
    new ParameterSubgraph(parameterQuery).subgraph(temporalGraph)

}
