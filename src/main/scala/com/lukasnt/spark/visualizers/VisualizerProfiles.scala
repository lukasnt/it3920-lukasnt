package com.lukasnt.spark.visualizers

object VisualizerProfiles {

  val defaultProfile        = new TemporalGraphProfile()
  val defaultGenericProfile = new GenericGraphProfile()

}
